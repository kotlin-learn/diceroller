package com.pakawat.diceroller

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import org.w3c.dom.Text

/**
 * This activity allow the user to roll a dice and view the result
 * on the screen
 */
class MainActivity : AppCompatActivity() { //inherit หรือ extends นั่นแหล่ะ
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main) //R= folder res

        val rollButton: Button = findViewById(R.id.button) //ดึงปุ่ม Button
        rollButton.setOnClickListener {
            val toast =
                Toast.makeText(this, "Dice Rolled!", Toast.LENGTH_SHORT) //สร้าง Toast object
            toast.show() //แสดง toast

            rollDice()
        }
    }

    /**
     * Row the dice and update the screen with the result
     */
    private fun rollDice() {
        //create new Dice object with 6 sides
        val dice = Dice(6)
//        row the dice
        val diceRoll = dice.roll()

        // update the screen with the result dice Roll
        val imgEl: ImageView = findViewById(R.id.imageView)
        val drawableResouce = when(diceRoll){
            1->R.drawable.dice_1
            2->R.drawable.dice_2
            3->R.drawable.dice_3
            4->R.drawable.dice_4
            5->R.drawable.dice_5
            6->R.drawable.dice_6
            else -> R.drawable.dice_6
        }
        imgEl.setImageResource(drawableResouce)
        imgEl.contentDescription = diceRoll.toString()

    }
}